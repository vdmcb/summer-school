#include <iostream>
#include <conio.h>
//typedef bool(*fctPointer)(const int&, const int&);
using fctPointer = bool(*)(const int&, const int&);
bool comparatorDesc(const int &a, const int &b)
{
	return a < b;
}
int cifrePare(const int &numar)
{
	int auxVal = numar;
	int uCifra, contor = 0;
	while (auxVal)
	{
		uCifra = auxVal % 10;
		if (uCifra % 2 == 0)
			contor++;
		auxVal /= 10;

	}
	if (contor)
		return contor;
	return 0;
}
bool comparatorCifrePare(const int &a, const int &b)
{
	return cifrePare(a) > cifrePare(b);

}
void mySort(int *sirNumere, const int &size, fctPointer comparator)
{
	int auxVal;
	for (int i = 1; i < size; i++)
	{
		for (int j = 0; j < size-i; j++) {
			if (comparator(sirNumere[j], sirNumere[j+1]))
			{
				auxVal = sirNumere[j];
				sirNumere[j] = sirNumere[j+1];
				sirNumere[j+1] = auxVal;

			}
		}
	}
}
typedef double(*Operation)(const double&, const double&);
double Suma(const double &param1, const double &param2)
{
	return param1 + param2;
}
double Produs(const double &param1, const double &param2)
{
	return param1*param2;
}
double Impartire(const double &param1, const double &param2)
{
	return param1 / param2;
}
double Scadere(const double &param1, const double &param2)
{
	return param1 - param2;
}
double GetResult(Operation operation, const double &param1, const double &param2)
{
	return operation(param1, param2);
}
int main() {
	int n, choice = 1;
	double val1, val2;
	std::cin >> n;
	int *sirNumere = new int[n];
	for (int i = 0; i < n; i++)
	{
		std::cin >> sirNumere[i];
	}
	mySort(sirNumere, n,comparatorCifrePare);
	for (int i = 0; i < n; i++)
		std::cout << sirNumere[i] << "   ";
	//std::cout << "Cele doua valori: ";
	//std::cin >> val1 >> val2;

	//do
	//{
	//	
	//	std::cout << "\nCele doua numere: "<<val1<<"   |   "<<val2<<"\n1.Suma\n2.Scadere\n3.Inmultire\n4.Impartire\n0.Paraseste programul.\n";

	//	std::cin >> choice;
	//	system("CLS");
	//	switch (choice)
	//	{
	//	case 1:
	//	{
	//		std::cout <<"Rezultat: "<< GetResult(Suma, val1, val2);
	//		break;
	//	}
	//	case 2:
	//	{
	//		std::cout << "Rezultat: " << GetResult(Scadere, val1, val2);
	//		break;
	//	}
	//	case 3:
	//	{
	//		std::cout << "Rezultat: " << GetResult(Produs, val1, val2);
	//		break;
	//	}
	//	case 4:
	//	{
	//		std::cout << "Rezultat: " << GetResult(Impartire, val1, val2);
	//		break;
	//	}
	//	case 0:
	//		break;
	//	default:
	//		std::cout << "Alegere invalida!" << std::endl;

	//	}

	//	/*	std::cout << "\n1.Suma\n2.Scadere\n3.Inmultire\n4.Impartire\n0.Paraseste programul.\n";
	//		std::cin >> choice;*/

	//} while (choice != 0);


	_getch();
	return 0;
}