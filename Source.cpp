#include <iostream>
#include <conio.h>
#include <random>
#include <time.h>
#include <vector>
#include "Punct.h"
using namespace std;

double distanta(float x, float y, int x1, int y1)
{
	return sqrt((x1 - x)*(x1 - x) + (y1 - y)*(y1 - y));
}


int main() {
	int nrPuncte;
	int laturaPatrat;
	int raza,cercX,cercY;
	int contorPatrat = 0, contorCerc = 0;
	cout << "Numarul de puncte: ";
	cin >> nrPuncte;
	cout << "Latura patratului: ";
	cin >> laturaPatrat;
	raza = laturaPatrat / 2;
	cercX = raza;
	cercY = raza;
	vector<Punct>ListaPuncte;
	srand(time(NULL));
	for (int i = 0; i < nrPuncte; i++) {
		/*double x = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
		double y = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);*/
		random_device rd;
		mt19937 mt(rd());
		uniform_real_distribution<float> rand(0, laturaPatrat+1);
		float x = (float)rand(mt);
		float y = (float)rand(mt);
		Punct b(x, y);
		ListaPuncte.push_back(b);
		double CalcDist = distanta(ListaPuncte[i].getX(), ListaPuncte[i].getY(), cercX, cercY);
		if (CalcDist > raza)
			contorPatrat++;
		else
			contorCerc++;

	}
	double pi = (double)contorCerc / nrPuncte * 4.0;
	cout << "Pi= " <<pi<< endl;


	_getch();
	return 0;
}