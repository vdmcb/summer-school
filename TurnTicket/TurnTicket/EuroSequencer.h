#pragma once
#include "ISequencer.h"
#include <memory>

class EuroSequencer:public ISequencer
{
private:
	EuroSequencer():ISequencer("EURO"){};
	EuroSequencer(const EuroSequencer&) = delete;
	void operator=(const EuroSequencer&) = delete;
public:
	static std::shared_ptr<EuroSequencer> getInstance();
	~EuroSequencer();
};

