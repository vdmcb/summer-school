#include "ISequencer.h"

int ISequencer::getNextTurnNumber()
{
	//std::cout << m_turnNumber << std::endl;
	return m_turnNumber++;
}

ISequencer::~ISequencer()
{
}

std::string ISequencer::getType() const
{
	return m_type;
}
