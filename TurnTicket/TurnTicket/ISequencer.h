#pragma once
#include <iostream>
class ISequencer
{
	int m_turnNumber = 0;
protected:
	std::string m_type;
public:

	virtual int getNextTurnNumber();
	ISequencer(const std::string type) :m_type(type) {};
	std::string getType()const;
	virtual ~ISequencer();

};

