#pragma once
#include "ISequencer.h"
#include "memory"

class LeiSequencer:public ISequencer
{
private:
	LeiSequencer():ISequencer("LEI"){};
	LeiSequencer(const LeiSequencer&) = delete;
	void operator=(const LeiSequencer&) = delete;
public:
	static std::shared_ptr<LeiSequencer> getInstance();
	~LeiSequencer();

};

