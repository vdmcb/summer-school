#pragma once
#include "ISequencer.h"
#include "TurnTicket.h"
#include <memory>
template <typename  T>
class SequencerOfType
{
	std::shared_ptr<T> m_sequencer;
	
public:
	TurnTicket getNextTicket()const
	{
		return TurnTicket(m_sequencer->getNextTurnNumber(), m_sequencer->getType());
	}
	explicit SequencerOfType(const std::shared_ptr<T>& sequencer):m_sequencer(sequencer){}
	~SequencerOfType(){}
};


