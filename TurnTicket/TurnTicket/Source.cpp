

#include "TurnNumberSequence.h"
#include <iostream>
#include "TicketDispenser.h"
#include "LeiSequencer.h"
#include "EuroSequencer.h"
#include "SequencerOfType.h"

auto main()->int
{
	/*TicketDispenser newTicketDispenser;

	auto newTurnTicket = newTicketDispenser.getTurnTicket();
	auto otherTurnTicket = newTicketDispenser.getTurnTicket();

	newTurnTicket.getTurnNumber();
	otherTurnTicket.getTurnNumber();
	
	*/
	auto newLeiSeq = LeiSequencer::getInstance();
	auto newEuroSeq = EuroSequencer::getInstance();
	auto a = LeiSequencer::getInstance();
	//a->getNextTurnNumber();
	auto b = LeiSequencer::getInstance();

	SequencerOfType<EuroSequencer> euroSeq(newEuroSeq);
	SequencerOfType<LeiSequencer> newSequencerOfTypeLei(newLeiSeq);
	SequencerOfType<LeiSequencer> newS(a);
	auto x = newSequencerOfTypeLei.getNextTicket();
	std::cout << x.getTurnNumber() << " " << x.getTurnType() << std::endl;
	auto y = newSequencerOfTypeLei.getNextTicket();
	std::cout << y.getTurnNumber() << " " << y.getTurnType() << std::endl;
	auto z = newS.getNextTicket();
	std::cout << z.getTurnNumber() << " " << z.getTurnType() << std::endl;
	//newLeiSeq->getNextTurnNumber();
	//newLeiSeq->getNextTurnNumber();
	//std::cout << "EURO" << std::endl;
	//newEuroSeq->getNextTurnNumber();

	//std::cout << a.use_count();
	//std::cout << newEuroSeq.use_count();

	return 0;
}
