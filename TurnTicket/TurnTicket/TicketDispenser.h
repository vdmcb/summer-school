#pragma once
#include "TurnTicket.h"
#include <memory>
#include "ISequencer.h"

class TicketDispenser
{

public:
	TicketDispenser();
	~TicketDispenser();

	TurnTicket getTurnTicket(const std::shared_ptr<ISequencer>);
};

