#include "TurnNumberSequence.h"
std::shared_ptr<TurnNumberSequence> TurnNumberSequence::getInstance()
{
	
	static std::shared_ptr<TurnNumberSequence> instance(new TurnNumberSequence);
	return instance;
}

int TurnNumberSequence::getNextTurnNumber()
{
	std::cout << turnNumber << "   ";
	return	turnNumber++;
}


TurnNumberSequence::~TurnNumberSequence()
{
}

