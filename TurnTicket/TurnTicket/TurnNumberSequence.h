#pragma once
#include <iostream>
#include <memory>

class TurnNumberSequence
{
private:
	TurnNumberSequence(){}
	int turnNumber=0;

public:
	static std::shared_ptr<TurnNumberSequence> getInstance();
	 int getNextTurnNumber();
	 TurnNumberSequence(const TurnNumberSequence&) = delete;
	 void operator=(const TurnNumberSequence&) = delete;
	~TurnNumberSequence();
};

