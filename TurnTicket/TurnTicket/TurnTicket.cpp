#include "TurnTicket.h"



TurnTicket::TurnTicket(const int turnNumber, const std::string& type) : m_turnNumber(turnNumber), m_type(type) {}


TurnTicket::~TurnTicket()
{
}

int TurnTicket::getTurnNumber() const
{
	return m_turnNumber;
}

std::string TurnTicket::getTurnType() const
{
	return m_type;
}

