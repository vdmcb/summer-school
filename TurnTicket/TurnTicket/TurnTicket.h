#pragma once
#include <string>
class TurnTicket
{
private:
	const int m_turnNumber;
	std::string m_type;
public:
	TurnTicket(const int,const std::string&);
	~TurnTicket();
	int getTurnNumber()const;
	std::string getTurnType()const;
};

