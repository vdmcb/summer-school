#include "Ex_Addresses.h"
#include <iostream>
#include <conio.h>

namespace Ex
{
    void VisualizeAddressesInMemory()
    {
        auto a = 5;
        auto b = 5.0;
        const auto cb = 5.0;
        auto c = 6L;
        auto d = 7U;
        auto e = 'a';
        auto f = "str";

        // 1.0 What are the addresses of the previous declared variables? (using VS Debug)
        // 1.1 Run the program multiple times, do their address change ? Why ?

        // 2. What are the sizes of the variables previusly declared ?

        // 3. What are the sizes of their addresses ?

        // 4. Write a function that display <variable_value> : <variable_address> and use it to display information for previously declared variables

        // 5. Change target platform for compiling. What is the difference between x86 & x64?

        // 6.0 Swap the contents of two integer variables.
        // 6.1 Write a function that does that.

		std::cout << a << "   --------   " << &a
			<< "\n" << b << "   --------   " << &b
			<< "\n" << c << "   --------   " << &c
			<< "\n" << d << "   --------   " << &d
			<< "\n" << e << "   --------   " <<static_cast<void *>( &e)
			<< "\n" << f << "   --------   " << &f<<"\n";

		std::cout << "sizeof var: " << sizeof(a) << "     sizeof adr: " << sizeof(&a) << std::endl;
		std::cout << "sizeof var: " << sizeof(b) << "     sizeof adr: " << sizeof(&b) << std::endl;
		std::cout << "sizeof var: " << sizeof(c) << "     sizeof adr: " << sizeof(&c) << std::endl;
		std::cout << "sizeof var: " << sizeof(d) << "     sizeof adr: " << sizeof(&d) << std::endl;

    }

}
template<class T>
void swap(const T& a, const T& b)
{
	T aux = a;
	a = b;
	b = aux;
}
int main() {

	Ex::VisualizeAddressesInMemory();
	_getch();
	return 0;
}