#include <iostream>
#include <string>
#include <fstream>
#include <conio.h>
using namespace std;
int checkTxt(const std::string &nume)
{
	const char *numeC_string = nume.c_str();
	int lungimeNume = strlen(numeC_string);
	const char *extensieFis = &(numeC_string[lungimeNume-3]);
	const char *extensie = "txt";
	if (strncmp(extensie, extensieFis,3)==0)
	{
		return 1;
	}

	return 0;
}

int main()
{
	cout << "Input file name:" << endl;
	string szFileName;
	cin >> szFileName;
	cout << "File name to open: " << szFileName << endl;
	
	ifstream file;
	if (checkTxt(szFileName))
	{
		file.open(szFileName);
		char szTextLine[64];

		while (file.getline(szTextLine, 64)) {
			cout << ">" << szTextLine << endl;
		}
		file.close();
	}
	else
		std::cout << "Fisierul nu este de tip txt!" << std::endl;
	cout << "Input file name:" << endl;
	string szFileName2;
	cin >> szFileName2;
	cout << "File name to open: " << szFileName2 << endl;
	
	if (checkTxt(szFileName2))
	{
		ofstream file2;
		file2.open(szFileName2);
		if (file2.is_open()) {
			file2 << "Let this be the file contents!";
			file2.close();
		}
		else
			cout << "Could not open file: " << szFileName << endl;
		file.close();
	}

	else
		std::cout << "Fisierul nu este de tip txt!" << std::endl;
	
	_getch();
	return 0;
}