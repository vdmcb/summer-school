#include <iostream>
#include <string>
#include <sstream>
#include <conio.h>
#include <iomanip>
#include <sstream>
#include <string>
#define  W 10
int countDigits(int value)
{
	int counter = 0;
	while (value)
	{
		value /= 10;
		counter++;
	}
	return counter;
}
int main() {

	int nrLinii, nrColoane,nrDeplasare;
	std::cout << "Numarul de linii: ";
	std::cin >> nrLinii;
	std::cout << "Numarul de coloane: ";
	std::cin >> nrColoane;
	nrDeplasare = countDigits(nrColoane);
	float *matrice = new float[nrLinii*nrColoane];
	for (auto i = 0; i < nrLinii; i++)
		for (auto j = 0; j < nrColoane; j++)
		{
			std::cin >> matrice[i*nrColoane + j];
		}
	std::cout << std::right;
	std::cout << std::setw(W) <<"";
	for (int i = 0; i < nrColoane; i++)
	{
		std::cout << std::right;
		if(nrColoane<100)
		std::cout << std::setfill('.')<< std::setw(W-nrDeplasare-1) << "[" << i << "]";
	}
	std::cout << std::endl;
	std::cout << std::endl;

	for (int i = 0; i < nrLinii; i++) {
		std::cout <<std::setw(W - nrDeplasare - 1)<< "[" << i << "]";
		for (int j = 0; j < nrColoane; j++)
		{
			std::cout << std::right;
			std::cout << std::setprecision(3)<<std::setw(W) << std::setfill('.')<<matrice[i*nrColoane + j];
			//std::cout << std::setw(10) << "";
		}
		std::cout << std::endl;
		std::cout << std::endl;
	}

	delete[] matrice;
	_getch();
	return 0;
}