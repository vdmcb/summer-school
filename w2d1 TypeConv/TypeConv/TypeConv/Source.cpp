#include <iostream>
#include <conio.h>
#include <iomanip>
#include <string>
#include <chrono>
int functieRepetitiva(int &nivel,std::chrono::time_point<std::chrono::system_clock> start)
{
	std::chrono::time_point<std::chrono::system_clock> end;
	if (nivel == 1000)
		throw "Error!";
	nivel++;
	end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
	std::cout << "Nivelul " << nivel << std::endl << "Durata: " << elapsed_seconds.count() << std::endl;
	while (nivel < 10000)
	{
		functieRepetitiva(nivel,start);

	}
	return nivel;

}

int main() {
	int count = 0;
	std::chrono::time_point<std::chrono::system_clock> start;
	start = std::chrono::system_clock::now();
	
	try
	{
		
		functieRepetitiva(count,start);

	}
//	diff = (static_cast<float>(t2) - static_cast<float>(t1));
	catch (...)
	{

		std::cout << "ERROR" << std::endl << std::endl;
	}
	_getch();
	return 0;
}