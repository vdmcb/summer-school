#include <iostream>
#include <conio.h>
void f(int) { std::cout << "i"; }
void f(float) { std::cout << "f"; }
void f(double) { std::cout << "d"; }

int main() {
	f(1);
	_getch();
	return 0;
}
