#include <iostream>
#include <conio.h>

int main() {
	int x = 10;
	char *check = reinterpret_cast<char*>(&x);
	if (*(check) == x)
	{
		std::cout << "Little Endian !";
	}
	else
	{
		std::cout << "Big Endian !";
	}

	_getch();
	return 0;
}