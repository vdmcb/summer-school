#include <iostream>
#include <conio.h>
//#include "Header.h"
#include "variables.cpp"

//int myGlobalVar = 1313;
void ConstCastMe(const int& param)
{
	int& value = const_cast<int&>(param);
	value++;
}
void callMeAnyNumberOfTimes()
{
	static int count = 0;
	count++;
	std::cout << count << std::endl;
}
void printMyGlobalVar()
{
//	std::cout << globalVar << std::endl;
	std::cout << myGlobalVar << std::endl;

}
int main()
{
	int apples = 5;
	std::cout << apples << std::endl;

	ConstCastMe(apples);
	std::cout << apples << std::endl;
	/*callMeAnyNumberOfTimes();
	callMeAnyNumberOfTimes();
	callMeAnyNumberOfTimes();
	callMeAnyNumberOfTimes();
	callMeAnyNumberOfTimes();
	callMeAnyNumberOfTimes();*/
	printMyGlobalVar();
	
	_getch();
	return 0;
}