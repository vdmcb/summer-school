#include "ClockType.h"



ClockType::ClockType()
{
}


ClockType::~ClockType()
{
}

void ClockType::setTime(int hr, int min, int sec)
{
	this->hr = hr;
	if (min > 59)
	{
		for (int i = 0; i < (min / 60); i++)
		{
			incrementHours();
		}
		this->min = min % 60;
	}
	else
		this->min = min;
	if (sec > 59)
	{
		for (int i = 0; i < (sec / 60); i++)
		{
			incrementMinutes();
		}
		this->sec = sec % 60;
		/*	if (this->min > 59)
				setTime(this->hr, this->min, this->sec);*/
	}
	else
		this->sec = sec;

}
int ClockType::incrementSeconds()
{
	this->sec++;
	if (this->sec > 59)
	{
		this->sec = 0;
		incrementMinutes();
	}
	return this->sec;
}
int ClockType::incrementMinutes()
{
	this->min++;
	if (this->min > 59)
	{
		this->min = 0;
		incrementHours();
	}
	return this->min;
}

int ClockType::incrementHours()
{
	return this->hr++;
	
}
void ClockType::getTime(int& hr, int& min, int& sec) const
{
	hr = this->hr;
	min = this->min;
	sec = this->sec;
}
void ClockType::printTime()const
{
	std::cout << "ORA:  " << this->hr << "  MIN:  " << this->min << "  SEC:  " << this->sec << std::endl;
}
bool ClockType::equalTime(const ClockType& newClock)const
{
	int hr, min, sec;
	newClock.getTime(hr, min, sec);
	return (this->hr == hr && this->min == min &&this->sec == sec);
}