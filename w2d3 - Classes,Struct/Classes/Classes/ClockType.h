#pragma once
#include <iostream>
class ClockType
{
public:
	ClockType();
	~ClockType();

	void setTime(int, int, int);
	void getTime(int&, int&, int&) const;
	void printTime()const;
	int incrementSeconds();
	int incrementMinutes();
	int incrementHours();
	bool equalTime(const ClockType&)const;

private:
	int hr, min, sec;
};

