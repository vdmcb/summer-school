#pragma once
#include <malloc.h>
#include <iostream>
#include <algorithm>
class OrderedSetType
{
public:
	OrderedSetType(int n);
	OrderedSetType(const OrderedSetType& set);
	~OrderedSetType();
	int find(int element);
	void readSet();
	void display();
	void Clear();
	void insertElement(const int&);
	int *eraseElement(int x);
private:

	int* my_set;
	int n;

};

