#include "Student.h"



Student::Student()
{
}


Student::~Student()
{
}
std::string Student::getName() const { return this->nume; }
int Student::getAge() const { return this->varsta; }
std::string Student::getSpec() const { return this->specializare; }

void Student::citesteStudent()
{
	std::string num,spcz;
	int vrst;
	std::cout << "Numele: ";
	
	std::getline(std::cin, num);
	std::cout << "Varsta: ";
	std::cin >> vrst;
	std::cin.ignore();
	std::cout << "Specializare: ";
	std::getline(std::cin, spcz);
	this->nume = num;
	this->varsta = vrst;
	this->specializare = spcz;
}
void Student::afiseazaStudent() const
{
	std::cout << "Numele: " << this->nume << "  |  Varsta: " << this->varsta << "  |  Specializare: " 
				<< this->specializare << std::endl;
}