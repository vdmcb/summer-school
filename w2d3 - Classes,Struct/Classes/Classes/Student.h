#pragma once
#include <string>
#include <iostream>
class Student
{
public:
	Student();
	~Student();
	void citesteStudent();
	void afiseazaStudent() const;
	std::string getName() const;
	int getAge() const;
	std::string getSpec() const;


private:
	std::string nume;
	int varsta;
	std::string specializare;
};

