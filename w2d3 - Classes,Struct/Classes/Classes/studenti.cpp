#include <iostream>
#include <conio.h>
#include <vector>
#include <algorithm>
#include "Student.h"
typedef bool (*fctPointer)(const Student&, const Student&);
bool comparatorSortare(const Student& s1, const Student& s2)
{
	if (s1.getAge() != s2.getAge())
		return s1.getAge() < s2.getAge();
	else if (s1.getName() != s2.getName())
	{
			return (s1.getName().compare(s2.getName()) < 0);
	}
	else
	{
		return (s1.getSpec().compare(s2.getSpec()) < 0);
	}
}
void myInsertionSort(std::vector<Student>& Studenti,fctPointer operation)
{
	int size = Studenti.size();
	int j;
	for (int i = 0; i < size; i++)
	{
		j = i;
		while (j > 0 && operation(Studenti[j], Studenti[j - 1]))
		{
			std::swap(Studenti[j], Studenti[j - 1]);
			j--;
		}
	}

}
int main()
{
	int numarStudenti;
	std::cout << "Numarul de studenti: ";
	std::cin >> numarStudenti;
	std::cin.ignore();
	std::vector<Student> Studenti(numarStudenti);
	for (int i=0;i<numarStudenti;i++)
	{
		Studenti[i].citesteStudent();
	}
	//std::sort(Studenti.begin(), Studenti.end(),comparatorSortare);
	myInsertionSort(Studenti, comparatorSortare);
	for (auto i : Studenti)
	{
		i.afiseazaStudent();
	}



	_getch();
	return 0;
}