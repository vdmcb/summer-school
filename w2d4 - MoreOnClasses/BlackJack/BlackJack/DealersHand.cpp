#include "DealersHand.h"



DealersHand::DealersHand()
{
}


DealersHand::~DealersHand()
{
}
void DealersHand::addCardToHand(card& newCard)
{
		if (newCard.getSymbol() == 'A')
		{
			if (this->get_points() + HIGH_ACE_VALUE > 21) {
				this->totalPoints += LOW_ACE_VALUE;
				this->cardsInHand.insert(pair<card, int>(newCard, LOW_ACE_VALUE));
			}
			else {
				this->totalPoints += HIGH_ACE_VALUE;
				this->cardsInHand.insert(pair<card, int>(newCard, HIGH_ACE_VALUE));
			}
			if (this->totalPoints > 21)
				this->normalizeHand();
		}
		else
		{
			this->totalPoints += newCard.getRank();
			this->cardsInHand.insert(pair<card, int>(newCard, newCard.getRank()));
		}
		
}
