#include "Hand.h"



Hand::Hand()
{

}
Hand::~Hand()
{
}
void Hand::addCardToHand(card& newCard)
{
	int valueChosen;
	if (newCard.getSymbol() == 'A')
	{
		cout << "---PICK THE VALUE FOR YOUR ACE---\n    1/11    \n";
		cin >> valueChosen;
		while (valueChosen != 1 && valueChosen != 11)
		{
			cout << "INVALID VALUE!";
			cin >> valueChosen;
		}
		this->totalPoints += valueChosen;
		this->cardsInHand.insert(pair<card, int>(newCard, valueChosen));
	}
	else
	{
		this->totalPoints += newCard.getRank();
		this->cardsInHand.insert(pair<card, int>(newCard, newCard.getRank()));
	}

}
void Hand::normalizeHand()
{
	if (totalPoints > 21) {
		for (auto i : cardsInHand)
		{
			if (i.first.getSymbol() == 'A'&&i.second==HIGH_ACE_VALUE) {
				totalPoints = totalPoints - HIGH_ACE_VALUE + LOW_ACE_VALUE;
				i.second = LOW_ACE_VALUE;
			}
			if (totalPoints <= 21)
				break;
		}//AJUSTEAZA
	}
}
void Hand::displayHand()const
{
	for (auto i : this->cardsInHand)
	{
		i.first.nameCard(); cout << " | ";
	}
	cout<<"Score:  " << setw(2) << this->get_points() << " | ";
	cout << endl;
}
bool Hand::operator<(const Hand& b) const { return (this->totalPoints < b.get_points()); }
//int Hand::set_points(int x) { return totalPoints = x; }
int Hand::get_points()const { return totalPoints; }
