#pragma once
#include <map>
#include "card.h"
#include <iomanip>
#define HIGH_ACE_VALUE 11
#define LOW_ACE_VALUE 1
class Hand
{
protected:
	int totalPoints = 0;
	multimap<card,int> cardsInHand;

public:
	Hand();
	~Hand();
	/*int set_points(int x);*/
	int get_points()const;
	virtual void addCardToHand(card&);
	void normalizeHand();
	void displayHand()const;
	bool Hand::operator<(const Hand&)const;
};

