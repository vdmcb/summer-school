#include "card.h"

#define HIGH_ACE_VALUE 11

card::card(char x,int p)
{
	symbol = x;
	suit = p;
	rank = setValue();
	given = false;
}
card::card() {
	//default
}
int card::setValue()
{
	
	if (symbol == 'A')
	{
		return HIGH_ACE_VALUE;
	}
	else if (symbol == 'J' || symbol == 'Q' || symbol == 'K')
		return 10;
	else if (symbol == '0')
		return 10;
	else
		return(symbol - '0');
	
}
int card::getRank()const {
	return this->rank;
}
int card::getSuit() {
	return this->suit;
}
char card::getSymbol() const{
	return this->symbol;
}
bool card::ifGiven() {
	return this->given;
}
void card::setGiven(bool x) {
	this->given = x;
}
void card::nameCard()const
{
	string name;
	switch (suit)
	{
	case 0: name = "spades"; break;
	case 1: name = "clubs"; break;
	case 2: name = "hearts"; break;
	case 3: name = "diamonds";
	}
	if (symbol== 'A')       cout << setw(6) << "Ace";
	else if (symbol == 'J') cout << setw(6) << "Jack";
	else if (symbol == 'Q') cout << setw(6) << "Queen";
	else if (symbol == '0')  cout << setw(6) << "10";
	else                    cout << setw(6)<<rank;

	cout<< " of " <<setw(8)<< name;
}
bool card::operator<(const card& newCard) const{ return this->getRank() < newCard.getRank(); }

