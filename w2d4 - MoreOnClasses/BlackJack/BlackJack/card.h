#pragma once
#include <iostream>
#include <iomanip>
#include<string>
using namespace std;
class card
{
	
	int suit;
	char symbol;
	int rank;
	bool given;
	
	//ii da o valoare cartii;
	int setValue();
public:
	card();
	card(char, int);
	int getSuit();
	char getSymbol()const;
	int getRank()const;
	void nameCard()const;
	bool ifGiven();
	void setGiven(bool);
	bool operator<(const card&)const;
	
	
};

