#pragma once
#include "DealersHand.h"
class Dealer
{
	DealersHand dealersHand;
public:
	Dealer();
	~Dealer();
	bool askForDealersChoice();
	Hand& get_Hand();
	void displayScore();
};