#pragma once
#include <vector>
#include <algorithm>
#include <time.h>
#include "card.h"
using namespace std;
class deck: public card
{
	int currentCard;
	vector<card>C;
public:
	deck();
	void autoFill();
	void shuffle();
	card giveCard();
};

