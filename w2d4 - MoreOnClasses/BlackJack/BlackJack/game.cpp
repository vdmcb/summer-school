#include "game.h"
Game::Game() {
}

void Game::displayEveryPlayer()
{
	for (int i = 0; i < numberOfPlayers; i++)
	{
		cout << "Player " << i + 1 << " : ";
		player[i].get_Hand().displayHand();
	}
	cout << setw(11) << right << "Dealer: ";
	currentDealer.get_Hand().displayHand();
}
bool Game::allPlayersStand()
{
	int countStopped = 0;
	for (auto i : player)
	{
		if (i.get_stopped() == true)
			countStopped++;
	}
	if (countStopped == numberOfPlayers)
		return true;
	else
		return false;
}
void Game::computeResults()
{
	int numberOfWinners = 0, maximumScore = 0;
	sort(player.begin(), player.end());
	if (currentDealer.get_Hand().get_points() < 21) {
		for (auto i : player)
		{
			if (i.get_Hand().get_points() <= 21 && i.get_Hand().get_points() > currentDealer.get_Hand().get_points() &&
				i.get_Hand().get_points() >= maximumScore)
			{
				maximumScore = i.get_Hand().get_points();
				if (i.get_Hand().get_points() == maximumScore)
					numberOfWinners++;
			}

		}
		if (numberOfWinners > 1)
			cout << "SPLIT BETWEEN: ";
		else if (numberOfWinners == 1)
			cout << "WINNER: ";
		else
			cout << "THE DEALER WON!" << endl;
		for (auto i : player)
		{
			if (i.get_Hand().get_points() == maximumScore)
				cout << "PLAYER " << i.get_index() << "  ";
		}
		if (currentDealer.get_Hand().get_points() == maximumScore)
			cout << "DEALER " << endl;
	}
	else if (currentDealer.get_Hand().get_points() == 21)
	{
		numberOfWinners = 0;
		for (auto i : player)
		{
			if (i.get_Hand().get_points() == 21)
				numberOfWinners++;
		}
		if (numberOfWinners != 0)
		{
			cout << "SPLIT BETWEEN: DEALER ";
			for (auto i : player)
			{
				if (i.get_Hand().get_points() == 21)
					cout << "PLAYER " << i.get_index() << "  ";
			}
		}
		else
			cout << "THE DEALER WON!" << endl;
	}
	else
	{
		for (auto i : player)
		{
			if (i.get_Hand().get_points() <= 21 && i.get_Hand().get_points() >= maximumScore)
			{
				maximumScore = i.get_Hand().get_points();
				numberOfWinners++;
			}
		}
		if (numberOfWinners != 0)
		{
			if (numberOfWinners > 1)
				cout << "WINNERS: ";
			else if (numberOfWinners == 1)
				cout << "WINNER: ";
			for (auto i : player)
			{
				if (i.get_Hand().get_points() == maximumScore)
					cout << "PLAYER " << i.get_index() << " ";
			}
			cout << endl;
		}
		else
			cout << "EVERYONE LOST!" << endl;
	
	}
}
int Game::startGame() {

	int firstTwoIterations = 0, menuChoice, valueChosen;
	char ans;
	card carte;
	string nume;
	cout << "\n(1)JOACA BLACKJACK.\n(0)QUIT." << endl;
	cin >> menuChoice;
	while (menuChoice != 0 && menuChoice != 1)
	{
		cout << "INVALID OPTION>>>PICK AGAIN!" << endl;
		cin >> menuChoice;
	}
	cout << "NUMBER OF PLAYERS: ";
	cin >> numberOfPlayers;
	player.resize(numberOfPlayers);
	while (menuChoice != 0) {
		system("cls");
		while (firstTwoIterations < 2) {
			for (int i = 0; i < numberOfPlayers; i++) {
				system("CLS");
				displayEveryPlayer();
				cout << "PLAYER " << i + 1 << endl;
				player[i].set_index(i + 1);
				carte = myDeck.giveCard();
				player[i].get_Hand().addCardToHand(carte);
				system("CLS");
				displayEveryPlayer();
			}
			carte = myDeck.giveCard();
			currentDealer.get_Hand().addCardToHand(carte);
			currentDealer.get_Hand().normalizeHand();//???????????
			firstTwoIterations++;
			cout << "NEXT ROUND -> PUSH BUTTON TO CONTINUE." << endl;
			_getch();
			system("CLS");
		}
		
		do {
			for (int i = 0; i < numberOfPlayers; i++) {
				if (player[i].askPlayerForChoice()) {
					carte = myDeck.giveCard();
					player[i].get_Hand().addCardToHand(carte);
					system("CLS");
					this->displayEveryPlayer();
				}
				else {
					displayEveryPlayer();
				}
			}
			if (currentDealer.askForDealersChoice()) {
				carte = myDeck.giveCard();
				currentDealer.get_Hand().addCardToHand(carte);
				currentDealer.get_Hand().normalizeHand();
				system("CLS");
				this->displayEveryPlayer();
			}
			if (allPlayersStand()) {
				while (currentDealer.askForDealersChoice()) {
					carte = myDeck.giveCard();
					currentDealer.get_Hand().addCardToHand(carte);
					currentDealer.get_Hand().normalizeHand();
					system("CLS");
					this->displayEveryPlayer();
				}
			}
			system("CLS");
			this->displayEveryPlayer();
		} while (!allPlayersStand());
		computeResults();

		cout << "\n(1)JOACA BLACKJACK.\n(0)QUIT." << endl;
		cin >> menuChoice;
	}
	return 0;
}