#pragma once
#include "deck.h"
#include "player.h"
#include "dealer.h"
#include <string>
#include <algorithm>
#include <conio.h>

using namespace std;
class Game
{
	vector<Player> player;
	deck myDeck;
	Dealer currentDealer;
	unsigned int numberOfPlayers;
public:
	Game();
	int startGame();
	void displayEveryPlayer();
	bool allPlayersStand();
	void computeResults();
};

