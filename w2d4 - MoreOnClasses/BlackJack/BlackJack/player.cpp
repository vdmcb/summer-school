#include "player.h"



Player::Player()
{
	totalM = 0;
	stopped = false;
}


int Player::set_money(int x) {
	return totalM = x;
}
int Player::get_money() {
	return totalM;
}

int Player::set_index(int x) {
	return index = x;
}
int Player::get_index() {
	return index;
}
bool Player::operator<(Player& player) { return this->get_Hand().get_points() > player.get_Hand().get_points(); }
bool Player::askPlayerForChoice()
{
	cout << "PLAYER " << this->get_index() << endl;
	if (this->get_Hand().get_points() >= 21) {
		stopped = true;
		return false;
	}
	if (this->get_Hand().get_points() < 21&&this->stopped==false) {
		int choice;
		cout << "GET CARD? (H/S)" << endl;
		choice = _getch();
		do
		{
			if (choice == 'H' || choice == 'h')
			{
				return true;
			}
			else if (choice == 's' || choice == 'S')
			{
				stopped = true;
				return false;
			}
			else
				cout << "ALEGERE INVALIDA!" << endl;


		} while (choice != 'h' && choice != 'H' && choice != 's' && choice != 'S');
	}
	return false;
}
void Player::displayScore()
{
	cout << this->get_Hand().get_points();
}
Hand& Player::get_Hand() { return playersHand; }
bool Player::get_stopped() { return stopped; }
