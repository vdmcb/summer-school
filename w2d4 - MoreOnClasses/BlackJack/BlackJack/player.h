#pragma once
#include "Hand.h"
#include <conio.h>
class Player
{
	int totalM;
	bool playersChoice;
	bool stopped;
	int index;
	Hand playersHand;


	
public:
	Player();
	int set_index(int x);
	int get_index();
	int get_money();
	int set_money(int x);
	Hand& get_Hand();
	bool askPlayerForChoice();
	void displayScore();
	bool get_stopped();
	bool operator<(Player&);
};

