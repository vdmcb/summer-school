#include "Punct2D.h"



Punct2D::Punct2D(const double& x, const double& y, const int& size) :m_x(x), m_y(y), m_size(size)
{
	m_array = new int[size];
}
Punct2D::Punct2D() {}

Punct2D::~Punct2D()
{
	// if (m_array != nullptr)
		delete[] m_array;

}
Punct2D::Punct2D(Punct2D&& point) :m_x(point.m_x), m_y(point.m_y), m_size(point.m_size)
{
	this->m_array = point.m_array;
	point.m_array = nullptr;
	point.m_size = 0;
}
//Punct2D& Punct2D::operator=(const Punct2D& point)
//{
//	
//}
Punct2D& Punct2D::operator=(Punct2D&& point)
{
	this->m_x = point.m_x;
	this->m_y = point.m_y;
	this->m_size = point.m_size;
	this->m_array = point.m_array;
	point.m_array = nullptr;
	point.m_size = 0;
	return *this;
	
}
Punct2D::Punct2D(const Punct2D& punctNou) :m_x(punctNou.m_x), m_y(punctNou.m_y), m_size(punctNou.m_size)
{
	this->m_array = new int[this->m_size];
	for (int i = 0; i < punctNou.m_size; i++)
		this->m_array[i] = punctNou.m_array[i];
}

void Punct2D::print()
{
	std::cout << std::setprecision(2) << "X: " << this->m_x << "  |  Y: " << this->m_y << std::endl;
	std::cout << "\t" << m_size << " allocated at " << m_array << std::endl;
}

double Punct2D::distanceTo(const Punct2D& punctParametru)const
{
	return sqrt((this->m_x - punctParametru.m_x)*(this->m_x - punctParametru.m_x) + (this->m_y - punctParametru.m_y)*(this->m_y - punctParametru.m_y));
}