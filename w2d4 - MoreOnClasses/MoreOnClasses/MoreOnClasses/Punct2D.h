#pragma once
#include <iostream>
#include <iomanip>
class Punct2D
{
private:
	double m_x=0.0;
	double m_y=0.0;
	int* m_array=nullptr;
	int m_size = 0;

public:
	Punct2D(const double&,const double&,const int&);
	Punct2D();
	~Punct2D();
	Punct2D(const Punct2D&);
	Punct2D(Punct2D&& point);
	void print();
	double distanceTo(const Punct2D&)const;
	friend Punct2D duplicate(const Punct2D&);
	Punct2D& operator=(const Punct2D&);
	Punct2D& operator=(Punct2D&& point);
};

