#include <iostream>
#include <conio.h>
#include <vector>
struct Punct2Dstruct
{
	float x, y;
	Punct2Dstruct(const float& x=0,const float& y=0) :x(x), y(y) {};


};
struct DistantaMaxima
{
	Punct2Dstruct punct1;
	Punct2Dstruct punct2;
	//DistantaMaxima(const Punct2D& p1, const Punct2D& p2) :punct1(p1), punct2(p2) {};
	DistantaMaxima()
	{
		punct1.x = 0;
		punct1.y = 0;
		punct2.x = 0;
		punct2.y = 0;
		
	}
};
float computeDistance(const Punct2Dstruct& punct1,const Punct2Dstruct& punct2)
{
	return sqrt((punct2.x-punct1.x)*(punct2.x - punct1.x) + (punct2.y-punct1.y)*(punct2.y-punct1.y));
}
bool pointEqual(const Punct2D& p1, const Punct2D& p2)
{
	return computeDistance(p1, p2) < 0.000001;
}
int main()
{
	int numarPuncte;
	float distantaMaxima;
	float distantaCalculata;
	DistantaMaxima StructDistMax;
	std::cout << "Numar de puncte: ";
	std::cin >> numarPuncte;
	std::vector<Punct2D> Puncte(numarPuncte);
	distantaMaxima = std::numeric_limits<float>::lowest();
	for (int i = 0; i < numarPuncte; i++)
	{
		std::cout << "Punctul " << i + 1 << "   x: ";
		std::cin >> Puncte[i].x;
		std::cout << "y: ";
		std::cin >> Puncte[i].y;
	}
	
	for (int i = 0; i < numarPuncte; i++) {
		for (int j = i + 1; j < numarPuncte; j++)
		{
			distantaCalculata = computeDistance(Puncte[i], Puncte[j]);
			if (distantaCalculata> distantaMaxima)
			{
				StructDistMax.punct1.x = Puncte[i].x;
				StructDistMax.punct1.y = Puncte[i].y;
				StructDistMax.punct2.x = Puncte[j].x;
				StructDistMax.punct2.y = Puncte[j].y;
				distantaMaxima = distantaCalculata;
				
			}
		}
	}

	_getch();
	return 0;
}