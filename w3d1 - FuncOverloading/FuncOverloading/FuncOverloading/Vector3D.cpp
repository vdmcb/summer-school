#include "Vector3D.h"



Vector3D::Vector3D(double x,double y,double z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}


Vector3D::~Vector3D()
{
}

Vector3D& Vector3D::operator+(const Vector3D& obj)
{
	Vector3D result(this->x+obj.x,this->y+obj.y,this->z+obj.z);
	return result;
	
}
Vector3D& Vector3D::operator=(const Vector3D& obj)
{
	this->x = obj.x;
	this->y = obj.y;
	this->z = obj.z;
	return *this;
}
Vector3D& Vector3D::operator*(const int& value)
{
	this->x *= value;
	this->y *= value;
	this->z *= value;
	return *this;
}
double Vector3D::operator*(const Vector3D& obj)
{
	return this->x*obj.x + this->y*obj.y + this->z*obj.z;
}
std::ostream& operator<<(std::ostream& flux, const Vector3D& obj)
{
	flux << "( " << obj.x << ", " << obj.y << ", " << obj.z << " )" << std::endl;
	return flux;
}