#pragma once
#include <iostream>
class Vector3D
{
	double x, y, z;
public:
	Vector3D(double,double,double);
	~Vector3D();
	Vector3D& operator+(const Vector3D&);
	Vector3D& operator=(const Vector3D&);
	double operator*(const Vector3D&);
	Vector3D& operator*(const int&);
	friend std::ostream& operator<<(std::ostream&, const Vector3D&);
};

