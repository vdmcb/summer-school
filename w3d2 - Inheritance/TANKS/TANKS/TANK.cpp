#include "TANK.h"




TANK::TANK(const bool& tracked, const unsigned int& power, const unsigned int& weight, const bool& turretWGun, const bool& machineGun)
{
	this->tracks = tracked;
	this->enginePower = power;
	this->weight = weight;
	this->turretWGun = turretWGun;
	this->machineGun = machineGun;
}

TANK::~TANK()
{
}
