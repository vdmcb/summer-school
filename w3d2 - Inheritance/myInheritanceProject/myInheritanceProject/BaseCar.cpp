#include "BaseCar.h"



BaseCar::BaseCar(int wheels)
{
	/*std::cout << "CONSTRUCTOR BASECAR" << std::endl;*/
	this->m_wheelsCount = wheels;
}


BaseCar::~BaseCar()
{
	std::cout << "DESTRUCTOR BASECAR." << std::endl;
}

int BaseCar::GetWhelsCount()
{
	return this->m_wheelsCount;
}
void BaseCar::printCapabilities(std::ostream& o)
{
	o << "UNBREAKABLE." << std::endl;
	o << "CAN GO DOWNHILL." << std::endl;
}