#pragma once
#include <iostream>
class BaseCar
{
private:
	int m_wheelsCount;
	char initiala = 'c';
public:
	BaseCar(int wheels = 4);
	~BaseCar();
	void printCapabilities(std::ostream&);
	int GetWhelsCount();
};

