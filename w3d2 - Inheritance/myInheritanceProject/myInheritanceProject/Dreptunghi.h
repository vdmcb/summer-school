#pragma once
#include "Paralelogram.h"
class Dreptunghi:virtual public Paralelogram
{
public:
	Dreptunghi(int,int);
	~Dreptunghi();
	int GetLungime();
	int GetLatime();
};

