#include "Poligon.h"



Poligon::Poligon(int numberOfSides)
{
	this->numberOfSides = numberOfSides;
	this->Sides.resize(numberOfSides);
}


Poligon::~Poligon()
{
}
int Poligon::Perimeter()
{
	int perimeter = 0;
	for (auto i : Sides)
		perimeter += i;
	return perimeter;
}
int Poligon::getNumberOfSides() { return numberOfSides; }

void Poligon::SetSideLength(const int& index, const int& value)
{
	this->Sides[index] = value;
}