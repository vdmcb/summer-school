#pragma once
#include <vector>
class Poligon
{
protected:

public:
	int numberOfSides;
	std::vector<int> Sides;
	Poligon(int);
	~Poligon();
	int Perimeter();
	int getNumberOfSides();
	void SetSideLength(const int&,const int&);
};

