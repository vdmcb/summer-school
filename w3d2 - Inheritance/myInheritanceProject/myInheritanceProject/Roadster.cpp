#include "Roadster.h"



Roadster::Roadster(int wheels,int seats,RGB color):RoadsterBase(wheels,seats)
{
	/*std::cout << "CONSTRUCTOR ROADSTER" << std::endl;*/
	this->m_color.m_red = color.m_red;
	this->m_color.m_green = color.m_green;
	this->m_color.m_blue = color.m_blue;
}


Roadster::~Roadster()
{
	std::cout << "DESTRUCTOR ROADSTER." << std::endl;
}
RGB Roadster::GetColor()
{
	return this->m_color;
}
void Roadster::MorphToTricycle()
{
	int *baseAdress = (int*)this;
	/**baseAdress = 3;
	*(baseAdress + 1) = 1;*/
	std::cout << "CHAR: " <<static_cast<char>( *(baseAdress+1 ))<< std::endl;
	/*std::cout << *baseAdress << std::endl << *(baseAdress + 1);*/
}

void Roadster::printCapabilities(std::ostream& o)
{
	RoadsterBase::printCapabilities(o);
	std::cout<<"ATTRACTS ATTENTION." << std::endl;
}