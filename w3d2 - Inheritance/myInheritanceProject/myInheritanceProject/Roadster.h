#pragma once
#include "RoadsterBase.h"
#include "RGB.h"
class Roadster:public RoadsterBase
{
	RGB m_color;
public:
	Roadster(int wheels = 4,int seats=2,RGB color = RGB(255,0,0));
	~Roadster();
	RGB GetColor();
	void MorphToTricycle();
	void printCapabilities(std::ostream&);
};

