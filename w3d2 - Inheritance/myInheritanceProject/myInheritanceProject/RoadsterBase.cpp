#include "RoadsterBase.h"



RoadsterBase::RoadsterBase(int wheels,int seats): BaseCar(wheels)
{
	/*std::cout << "CONSTRUCTOR ROADSTRERBASE" << std::endl;*/
	this->m_seatsCount = seats;
}


RoadsterBase::~RoadsterBase()
{
	std::cout << "DESTRUCTOR ROADSTER BASE." << std::endl;
}
int RoadsterBase::GetSeatsCont()
{
	return this->m_seatsCount;
}
void RoadsterBase::printCapabilities(std::ostream& o)
{
	/*BaseCar::printCapabilities(o);*/
	std::cout << "CAN GO ANYWHERE AND CAN CARRY PASSENGERS." << std::endl;
}
