#pragma once
#include "BaseCar.h"
class RoadsterBase: public BaseCar
{
	int m_seatsCount;
public:
	RoadsterBase(int wheels = 4,int seats = 2);
	~RoadsterBase();
	int GetSeatsCont();
	void printCapabilities(std::ostream&);
};

