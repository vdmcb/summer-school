#include<iostream>
#include "Patrat.h"
#include "Trapez.h"

int main()
{
	{
		std::cout << "Creare poligon cu 3 laturi";
		int numarCerutDeLaturi = 3;
		Poligon a(numarCerutDeLaturi);

		int numarDeLaturi = a.getNumberOfSides();
		if (numarDeLaturi == numarCerutDeLaturi)
			std::cout << "PASSED!";
		else
			std::cout << "FAILED!";
		std::cout << std::endl;
	}

	{
		std::cout << "Setare laturi poligon cu 3 laturi si perimetru";
		int numarCerutDeLaturi = 3;
		Poligon a(numarCerutDeLaturi);

		int sideIndex = 0;
		double firstSideLength = 10;
		a.SetSideLength(sideIndex, firstSideLength);

		sideIndex++;
		double secondSideLength = 12;
		a.SetSideLength(sideIndex, secondSideLength);

		sideIndex++;
		//triangle inequality: side3 < side2 + side1
		double thirdSideLength = 12;
		a.SetSideLength(sideIndex, thirdSideLength);

		double expectedPerimeter = thirdSideLength + secondSideLength + firstSideLength;
		double poligonPerimeter = a.Perimeter();
		if (expectedPerimeter == poligonPerimeter)
			std::cout << "PASSED!";
		else
			std::cout << "FAILED!";
		std::cout << std::endl;
	}

	{
		std::cout << "Patratul este un dreptunghi cu laturile egale ";
		Patrat p(12);
		if (p.GetLungime() == p.GetLatime())
			std::cout << "PASSED!";
		else
			std::cout << "FAILED!";
		std::cout << std::endl;
	}

	{
		std::cout << "Perimetrul rombului ";
		double length = 12;
		int numberOfSides = 4;

		Romb p(length);

		double expectedPerimeter = numberOfSides * length;
		double actualPerimeter = p.Perimeter();

		if (expectedPerimeter == actualPerimeter)
			std::cout << "PASSED!";
		else
			std::cout << "FAILED!";
		std::cout << std::endl;
	}

	{
		std::cout << "Perimetrul patratului ";
		double length = 12;
		int numberOfSides = 4;

		Patrat p(length);

		double expectedPerimeter = numberOfSides * length;
		double actualPerimeter = p.Perimeter();

		if (expectedPerimeter == actualPerimeter)
			std::cout << "PASSED!";
		else
			std::cout << "FAILED!";
		std::cout << std::endl;
	}

	return 0;
}