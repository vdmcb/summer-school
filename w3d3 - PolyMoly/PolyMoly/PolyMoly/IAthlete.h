#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "IInterrogate.h"
#include "IParticipation.h"
#include "IMedals.h"
class IAthlete
{
public:
	IAthlete(){};
	virtual const IParticipation* AsParticipation()const = 0;
	virtual const IInterrogate* AsInterrogate()const = 0;
	virtual const IMedals* AsMedals()const = 0;
};

