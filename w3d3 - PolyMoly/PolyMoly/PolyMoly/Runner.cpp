#include "Runner.h"

Runner::~Runner()
{
}
void Runner::interrogate()const
{
	std::cout << "My name is " << this->name << ", I am from " << this->nationality << ", and I run." << std::endl;
}

const IMedals* Runner::AsMedals() const
{
	return static_cast<const IMedals*>(this);
}
const IParticipation* Runner::AsParticipation()const
{
	return static_cast<const IParticipation*>(this);
}
const IInterrogate* Runner::AsInterrogate() const
{
	return static_cast<const IInterrogate*>(this);
}
int Runner::Participation()const { return disciplines; }
int Runner::Medals()const { return medals; }