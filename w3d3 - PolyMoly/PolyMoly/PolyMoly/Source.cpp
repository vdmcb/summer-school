#include <iostream>
#include <typeinfo>
#include "TennisPlayer.h"
#include "Runner.h"
#include "Swimmer.h"
#include <vector>
void interrogatorFunc(const IInterrogate* athlete)
{
	athlete->interrogate();
	std::cout << std::endl;
}
void displayMedals(const IMedals* athlete)
{
	std::cout << "Medalii: " << athlete->Medals() << std::endl;
}
void displayDisciplines(const IParticipation* athlete)
{
	int disciplines = athlete->Participation();
	if (disciplines & IParticipation::Swimm_50m)
		std::cout << "Swiming 50 meters" << std::endl;
	if (disciplines & IParticipation::Swimm_100m)
		std::cout << "Swiming 100 meters" << std::endl;
	if (disciplines & IParticipation::Running_50m)
		std::cout << "Running 50 meters" << std::endl;
	if (disciplines & IParticipation::Running_100m)
		std::cout << "Running 100 meters" << std::endl;
	if (disciplines & IParticipation::Tennis_singles)
		std::cout << "Tennis singles" << std::endl;
	if (disciplines & IParticipation::Tennis_doubles)
		std::cout << "Tennis doubles" << std::endl;
}
void displayAthletes(std::vector<IAthlete*>& Athletes)
{
	for (auto i : Athletes)
	{
		interrogatorFunc(i->AsInterrogate());
		displayDisciplines(i->AsParticipation());
		displayMedals(i->AsMedals());
		std::cout << std::endl; std::cout << std::endl; std::cout << std::endl;
	}
}
int main()
{
	std::vector<IAthlete*> Athletes;
	Athletes.push_back(new Swimmer("Marcel", "Egypt",3,2));
	Athletes.push_back(new Runner(	"Pavel", "Romania",5,12));
	Athletes.push_back(new TennisPlayer("Ionut", "Germany", 1, 32));
	Athletes.push_back(new TennisPlayer("John", "USA", 7, 48));
	displayAthletes(Athletes);
	



	return 0;
}