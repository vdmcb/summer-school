#include "Swimmer.h"

Swimmer::~Swimmer()
{
}

void Swimmer::interrogate()const
{
	std::cout << "My name is " << this->name << ", I am from " << this->nationality << ", and I swim." << std::endl;
}

const IMedals* Swimmer::AsMedals() const
{
	return static_cast<const IMedals*>(this);
}
const IParticipation* Swimmer::AsParticipation()const
{
	return static_cast<const IParticipation*>(this);
}
const IInterrogate* Swimmer::AsInterrogate() const
{
	return static_cast<const IInterrogate*>(this);
}
int Swimmer::Participation()const { return disciplines; }
int Swimmer::Medals()const{return medals;}