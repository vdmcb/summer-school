#include "TennisPlayer.h"

TennisPlayer::~TennisPlayer()
{
}

void TennisPlayer::interrogate()const
{
	std::cout <<"My name is "<<this->name<<", I am from "<<this->nationality<<", and I play tennis." << std::endl;
}

const IMedals* TennisPlayer::AsMedals() const
{
	return static_cast<const IMedals*>(this);
}
const IParticipation* TennisPlayer::AsParticipation()const
{
	return static_cast<const IParticipation*>(this);
}
const IInterrogate* TennisPlayer::AsInterrogate() const
{
	return static_cast<const IInterrogate*>(this);
}
int TennisPlayer::Participation()const { return disciplines; }
int TennisPlayer::Medals()const { return medals; }