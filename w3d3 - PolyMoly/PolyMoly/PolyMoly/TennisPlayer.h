#pragma once
#include "IAthlete.h"
class TennisPlayer:public IAthlete, public IMedals, public IInterrogate, public IParticipation
{
	std::string name, nationality;
	int medals, disciplines;
public:
	TennisPlayer(const std::string& name, const std::string& country, const int& numOfMedals, const int& disciplines) :name(name), nationality(country),
		medals(numOfMedals), disciplines(disciplines) {};


	~TennisPlayer();

	void interrogate()const;
	int Participation()const;
	int Medals()const;
	const IParticipation* AsParticipation()const;
	const IInterrogate* AsInterrogate()const;
	const IMedals* AsMedals()const;
};

