#include "LeninMobil.h"

LeninMobil::~LeninMobil()
{
}

bool LeninMobil::checkNumOfWheels()const
{
	return this->WheelsOfLeninMobil.size() >= 2;
}
bool LeninMobil::checkExistanceOfTracks()const
{
	return this->TracksOfLeninMobil.size() >= 2;
}
void LeninMobil::printCapability()const
{
	if (checkNumOfWheels())
		std::cout << "Has good steering." << std::endl;
	if (checkExistanceOfTracks())
		TracksOfLeninMobil[0].printCapability();//check this
	EngineOfLeninMobil.printCapability();
}