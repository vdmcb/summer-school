#pragma once
#include "Body.h"
#include "Engine.h"
#include "Seat.h"
#include "Track.h"
#include "Wheel.h"
#include <vector>
class LeninMobil
{
	Body BodyOfLeninMobil;
	Engine EngineOfLeninMobil;
	std::vector<Seat> SeatsOfLeninMobil;
	std::vector<Track> TracksOfLeninMobil;
	std::vector<Wheel> WheelsOfLeninMobil;
	bool checkNumOfWheels()const;
	bool checkExistanceOfTracks()const;
public:
	LeninMobil(const std::vector<Seat>& Seats, const std::vector<Track>& Tracks, const std::vector<Wheel>& Wheels) : SeatsOfLeninMobil(Seats),
		TracksOfLeninMobil(Tracks), WheelsOfLeninMobil(Wheels) {};

	void printCapability()const;

	~LeninMobil();
};

