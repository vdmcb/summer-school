#include "LeninMobil.h"
#include <iostream>
void wheelFactory(std::vector<Wheel>& newWheels)
{
	int numOfWheels;
	std::cout << "Number of wheels to be made: ";
	std::cin >> numOfWheels;
	newWheels.resize(numOfWheels);
}
void trackFactory(std::vector<Track>& newTracks)
{
	int numOfTracks;
	std::cout << "Number of tracks to be made: ";
	std::cin >> numOfTracks;
	newTracks.resize(numOfTracks);
}
void seatFactory(std::vector<Seat>& newSeats)
{
	int numOfSeats;
	std::cout << "Number of seats to be made: ";
	std::cin >> numOfSeats;
	newSeats.resize(numOfSeats);
}
int main()
{
	std::vector<Wheel> wheelsOfLeninBatMobil;
	wheelFactory(wheelsOfLeninBatMobil);

	std::vector<Track> tracksOfLeninBatMobil;
	trackFactory(tracksOfLeninBatMobil);

	std::vector<Seat> seatsOfLeninBatMobil;
	seatFactory(seatsOfLeninBatMobil);

	LeninMobil newLeninMobil(seatsOfLeninBatMobil, tracksOfLeninBatMobil, wheelsOfLeninBatMobil);
	newLeninMobil.printCapability();
	std::cout << sizeof(newLeninMobil);

	


	return 0;
}