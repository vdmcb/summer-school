#include "Cluster.h"



Cluster::Cluster(const Pixel& newPixel)
{
	mR = mG = mB = 0;
	insertPixel(newPixel);
}


Cluster::~Cluster()
{
}
Cluster::Cluster()
{
	mR = mG = mB = 0;
}

void Cluster::calculateMean()
{
	double sumR=0, sumG=0, sumB=0;
	for (auto i = 0; i < pixelsInCluster.size(); ++i)
	{
		sumR += pixelsInCluster[i].r;
		sumG += pixelsInCluster[i].g;
		sumB += pixelsInCluster[i].b;
	}
	mR = sumR / pixelsInCluster.size();
	mG = sumG / pixelsInCluster.size();
	mB = sumB / pixelsInCluster.size();
}
void Cluster::insertPixel( const Pixel&newPixel)
{
	pixelsInCluster.push_back(newPixel);
	calculateMean();
}
bool Cluster::operator==(const Cluster& cluster)const
{
	return this->mR == cluster.mR&&this->mG == cluster.mG&&this->mB == cluster.mB;
}