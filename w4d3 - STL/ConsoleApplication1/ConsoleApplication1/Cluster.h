#pragma once
#include "Pixel.h"
#include <vector>
class Cluster
{
public:
	Pixel centru;
	std::vector<Pixel> pixelsInCluster;
	unsigned int r, g, b;
	double mR, mG, mB;
	Cluster(const Pixel&);
	~Cluster();
	Cluster();
	void insertPixel(const Pixel&);
	void calculateMean();
	bool operator==(const Cluster&)const;

};

