#include "Image.h"



Image::Image(const std::string& fileName)
{
	std::string input;

	std::ifstream file(fileName);
	int height, width;
	if (file.is_open()) {
		std::cout << "FISIER DESCHIS"<<std::endl;
		
		for (std::string line; std::getline(file, line); )
		{
			not_digit not_a_digit;
			std::string::iterator end = std::remove_if(line.begin(), line.end(), not_a_digit);
			std::string all_numbers(line.begin(), end);
			std::vector<int> foundNumbers;
			std::stringstream stream(all_numbers);
			int n = 0;
			while (stream >> n)
				foundNumbers.push_back(n);
			if (foundNumbers.size() == 2)
			{
				height = foundNumbers[0];
				width = foundNumbers[1];
			}
			else
			{
				Pixel newPixel;
				newPixel.x = foundNumbers[0];
				newPixel.y = foundNumbers[1];
				newPixel.r = foundNumbers[2];
				newPixel.g = foundNumbers[3];
				newPixel.b = foundNumbers[4];
				this->pixels.push_back(newPixel);
			}
		}

	}
	else
		throw "BAD PROGRAMMING";

}


Image::~Image()
{
}
