#pragma once
#include <vector>
#include "Pixel.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <locale>
#include "Cluster.h"
class Image
{
	struct not_digit {
		bool operator()(const char c) {
			return c != ' ' && !isdigit(c);
		}
	};
public:
	/*std::vector<std::vector<std::tuple<int, int, int>>> m_image;*/
	int currentPixelIndex;
	std::vector<Pixel> pixels;
	Image(const std::string&);
	~Image();
};

