#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <memory>
#include <set>
#include <string>
int main()
{
	//
	//	/*std::cout << vec[1];*/
	//
	//	std::list<int> list;
	//
	//	std::map<char, int> newMap;
	//	newMap.emplace('a', 10);
	//
	//	std::vector<int> vec;
	//	for (auto i = 0; i < 10; ++i)
	//	{
	//		vec.push_back(i);
	//	}
	//	std::vector<int>::iterator it = vec.begin();
	//	/*for (; it != vec.end(); ++it)
	//	{
	//		std::cout << *it << "   ";
	//	}
	//
	//	for (const auto& i : vec)
	//	{
	//		std::cout << std::endl << i << "   ";
	//	}
	//*/
	//	std::for_each(vec.begin(), vec.end(), [](int& a) {std::cout << a << std::endl; });
	//	//Lambda
	//	//[] - lista de capturare
	//	//() - lista de parametri
	//	//{} - corp
	//	//() - call
	//	[]() {}();
	//	int a = 0;
	//	//capturare prin adresa
	//	[&a]() {}();
	//	//capturam totul prin valoare 
	//	{
	//		int val = 0, val2 = 1;
	//		[=]() {}();// = -> acces prin valoare la toate var
	//	}
	//
	//	//capturam totul prin ref
	//	{
	//		[&]() {}();
	//	}
	//
	//
	/*typedef std::set<std::string> t_SetString;
	typedef std::map<std::unique_ptr<int>, std::set<std::string>> t_SetStringMap;
	typedef std::map<std::map<std::unique_ptr<int>, std::set<std::string>>, double> t_SuperMap;

	std::map<std::map<std::unique_ptr<int>, std::set<std::string>>, double> map;
	map.emplace([]() -> t_SetStringMap {
		t_SetStringMap ret;
		for (auto i = 0; i < 10; ++i) {
			ret.emplace(std::make_unique<int>(std::pow(i, 3)),
				t_SetString({ "Test" + std::to_string(i),"Test" + std::to_string(i + 1) }));
		}

		return ret;

	}(), 10);
	auto it = map.begin()->first.begin();
	for (; it != map.begin()->first.end(); ++it)
	{
		for (auto itIntermediate = it->second.begin(); itIntermediate != it->second.end(); ++itIntermediate)
		{
			if (*itIntermediate == "Test7" || *itIntermediate == "Test9")
				std::cout << *it->first << std::endl;
		}


	}*/
	std::vector<double> dVec;
	for (auto i = 0; i < 10; ++i)
	{
		dVec.push_back(2.456*i);
	}
	auto val = 18.7895;
	const auto& itLowerBound = std::lower_bound(dVec.begin(), dVec.end(), val);
	const auto& index = std::distance(std::begin(dVec),itLowerBound);
	for (auto i : dVec)
		std::cout << i << "   ";
	std::cout <<std::endl<<"val: "<<*itLowerBound<<"index: "<< index << std::endl;

	return 0;
}