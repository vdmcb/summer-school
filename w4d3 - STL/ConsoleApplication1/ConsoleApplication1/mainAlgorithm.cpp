#include <iostream>
#include <vector>
#include <map>
#include <string>
#include "Pixel.h"
#include "Cluster.h"
#include "Image.h"
#include <fstream>
void writeToFile(std::vector<Cluster>& clusters)
{
	
	std::ofstream file("image.txt");
	
		for (auto j : clusters[0].pixelsInCluster)
			file << "[ " << j.x << " , " << j.y << " ] = [" << 0 << " , " << 0 << " , " << 255 << " ]" << std::endl;
		for (auto j : clusters[1].pixelsInCluster)
			file << "[ " << j.x << " , " << j.y << " ] = [" << 0 << " , " << 255 << " , " << 0 << " ]" << std::endl;
		for (auto j : clusters[2].pixelsInCluster)
			file << "[ " << j.x << " , " << j.y << " ] = [" << 255 << " , " << 0 << " , " << 0 << " ]" << std::endl;
		for (auto j : clusters[3].pixelsInCluster)
			file << "[ " << j.x << " , " << j.y << " ] = [" << 0 << " , " << 255 << " , " << 255 << " ]" << std::endl;
		for (auto j : clusters[4].pixelsInCluster)
			file << "[ " << j.x << " , " << j.y << " ] = [" << 255<< " , " << 255 << " , " << 0 << " ]" << std::endl;
}
double distanceFromCluster(const Pixel& currentPixel, const Cluster& currentCluster) {
	return sqrt((currentCluster.mR - currentPixel.r)*(currentCluster.mR - currentPixel.r) +
		(currentCluster.mG - currentPixel.g)*(currentCluster.mG - currentPixel.g) +
		(currentCluster.mB - currentPixel.b)*(currentCluster.mB - currentPixel.b));
}
void minDistance( Pixel currentPixel, std::vector<Cluster>& clusters,Image& myImage)
{
	double prevDistance = HUGE_VAL;
	Cluster* clusterPtr;
	for (auto& i : clusters) {
		if (distanceFromCluster(currentPixel, i) < prevDistance)
		{
			clusterPtr = &i;
			prevDistance = distanceFromCluster(currentPixel, i);
		}
	}
	/*for (auto i : clusters)
	{
		if (i == *clusterPtr)
			i.insertPixel(currentPixel);
	}*/
	clusterPtr->insertPixel(currentPixel);
	myImage.currentPixelIndex++;
	
}
int main()
{
	Image myImage("pozaMare.txt");
	int numberOfClusters,numberOfIterations =0 ;
	std::cout << "Number of clusters: ";
	std::cin >> numberOfClusters;
	std::vector<Cluster> clusters(numberOfClusters);
	for (auto i = 0; i < numberOfClusters; ++i)
	{
		clusters[i].insertPixel(myImage.pixels[i]);
		myImage.currentPixelIndex = i + 1;
	}
	while (numberOfIterations < 1) {
		for (auto i = myImage.currentPixelIndex; i < myImage.pixels.size(); ++i)
		{
			minDistance(myImage.pixels[i], clusters, myImage);
			
		}
		++numberOfIterations;
		std::cout << std::endl << "ITERATIA " << numberOfIterations << std::endl;
		myImage.currentPixelIndex = 3;
		for (auto i : clusters)
		{
			i.pixelsInCluster.clear();
		}

	}
	writeToFile(clusters);
	return 0;
}