#pragma once
#include <string>
#include <thread>
#include <iostream>
#include "Storage.h"


class Cons
{
	void consume()
	{
		//consume data
		std::string res;
		/*const auto success = sharedStorage->check_and_get(res);
		if (success)
		{
			std::cout << "Consumer: " << std::this_thread::get_id() << " ## " << res << std::endl;
		}
		else
		{
			std::cout << "Consumer: " << std::this_thread::get_id() << " ## NO DATA AVAILABLE" << std::endl;
		}*/
		while(!sharedStorage->check_and_get(res))
		{ 
			//if(success){
			
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			std::cout << "NO DATA AVAILABLE" << std::endl;
			//std::this_thread::sleep_for(std::chrono:;milliseconds(100));
			//no need because of captured true event
			//## even while can be if ##
		}
		std::cout << "Consumer " << std::this_thread::get_id()
			<< " ## " << res << std::endl;

	}
public:
	XStorage sharedStorage;
	std::thread th;

	Cons(XStorage _storage) : sharedStorage(_storage)
	{

	}
	~Cons()
	{
		if (th.joinable())
		{
			th.join();
		}
	}
	void launch_production()
	{
		th = std::thread(&Cons::consume, this);
	}
};
