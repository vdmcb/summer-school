#pragma once
#include <sstream>
#include "Storage.h"
#include <thread>
#include <iostream>


class Prod
{
public:
	std::thread th;
	XStorage sharedStorage;
	///////////////////////
	explicit Prod(XStorage _storage) :sharedStorage(_storage)
	{

	}
	~Prod()
	{
		if (th.joinable())
			th.join();
	}

	void launchProduction()
	{
		th = std::thread(&Prod::produce, this);
	}

private:
	void produce()const
	{
		for (auto i = 0;; ++i)
		{
			std::stringstream ss;
			ss << "Data " << i + 1 << "Produced by " << std::this_thread::get_id();
			if (sharedStorage->can_store())
			{

				sharedStorage->store(ss.str());
			}
			else
			{
				std::this_thread::sleep_for(std::chrono::milliseconds(100));
			}
			/*std::cout << "Producer " << std::this_thread::get_id() << " ## " << std::endl;*/

		}
	}
};
