#pragma once
#include <queue>
#include <string>
#include <mutex>
#include <iostream>


class Storage
{
public:
	Storage(int _max_size):max_size(_max_size)
	{
		
	}
	bool can_store()
	{
		std::unique_lock<std::mutex> lockGuard(mtx);
		return data.size() < max_size;
	}
	void store(std::string&& s)
	{
		std::cout << s.c_str();
		std::unique_lock<std::mutex> lockGuard(mtx);
		cond_var.wait(lockGuard, [this]() {return data.size() < max_size; });
		data.push(std::move(s));
		cond_var.notify_one();//notify_all???
	}

	bool check_and_get(std::string& outData)
	{
		std::unique_lock<std::mutex> lockGuard(mtx);
		cond_var.wait(lockGuard, [this]() {return data.size() >0; });

		outData = data.front();
		data.pop();
		return true;
	}
private:
	const int max_size;
	std::queue<std::string> data;
	std::mutex mtx;
	std::condition_variable cond_var;

};

typedef std::shared_ptr<Storage> XStorage;
