#pragma once
#include <mutex>
#include <iostream>

struct SyncCout:public std::basic_ostream<char>
{
public:
	
	template<typename T>
	SyncCout& operator<<(const T& value)
	{
		std::unique_lock<std::mutex> lck(mtx);
		std::cout << value;
		return *this;
	}

private:
	static std::mutex mtx;
};

std::mutex SyncCout::mtx;

static SyncCout scout;
