

#include "Prod.h"
#include "Cons.h"

auto main() -> int
{
	auto xs = std::make_shared<Storage>(10);
	Prod p(xs);

	Cons c(xs);

	p.launchProduction();

	c.launch_production();

	return 0;
}
