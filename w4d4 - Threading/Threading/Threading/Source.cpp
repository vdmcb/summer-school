

#include <iostream>
#include <thread>
#include <string>
#include "Summ.h"
#include <vector>
#include <sstream>
#include <mutex>

void threadFunc(const std::string& name,DoubleSumm& dSum)
{
	DoubleSumm copySumm;
	for(auto i=0;i<100000;++i)
	{
	
		
		copySumm += 1.;
		
	
	}
	dSum += copySumm;
}
int main()
{
	const auto proc_count = std::thread::hardware_concurrency();
	std::cout << proc_count << std::endl;
	DoubleSumm doubleSumm;

	

	std::vector<std::thread> vecTh(proc_count - 1);
	for(auto i=0;i<proc_count-1;++i)
	{
		std::stringstream sstr;
		sstr << "Thread" << (i + 1);
		vecTh[i] = std::thread(threadFunc, sstr.str(), std::ref(doubleSumm));
	}
	
	threadFunc("main",std::ref(doubleSumm));
	for (auto i = 0; i < proc_count-1; ++i)
		vecTh[i].join();

	std::cout << "Result is " << doubleSumm << std::endl;
	
	return 0;
}