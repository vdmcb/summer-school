#pragma once
#include <mutex>
template<typename T>
struct TSumm
{
	TSumm():val(T())
	{	
	}
	TSumm(const TSumm& that):val(that.val)
	{
	}
	TSumm(TSumm&& that):val(std::move(that.val))
	{
	}
	TSumm& operator+=(const T& _val)
	{
		//acquire exclusive access to common resource
		mtx.lock();
		val += _val;
		mtx.unlock();
		//releasing the exclusive access
		return *this;
	}
	operator T() const
	{
		return val;
	}

	TSumm& operator=(const TSumm& that)
	{
		this->val = that.val;
		return *this;
	}
private:
	T val;
	std::mutex mtx;
};
typedef TSumm<double> DoubleSumm;