#include "CReader.h"
#include "CReaderImpl.h"



CReader::CReader(CReaderImpl* impl) : m_impl(impl)
{
}


CReader::~CReader()
{
}

std::string CReader::getName() const
{
	return m_impl->getName();
}

std::string CReader::getUnit() const
{
	return m_impl->getUnit();
}

std::string CReader::getData() const
{
	return m_impl->getData();
}
