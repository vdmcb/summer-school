#pragma once
#include <memory>
#include <string>

class CReaderImpl;

class CReader
{
public:
	CReader(CReaderImpl* impl);
	~CReader();

	std::string getName() const;
	std::string getUnit() const;
	std::string getData() const;
private:
	CReaderImpl* m_impl;
};

