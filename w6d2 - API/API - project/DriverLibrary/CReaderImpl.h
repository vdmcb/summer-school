#pragma once
#include <string>

class CReaderImpl
{
public:
	CReaderImpl();
	virtual ~CReaderImpl();

	virtual std::string getName() const = 0;
	virtual std::string getUnit() const = 0;
	virtual std::string getData() const = 0;
};

