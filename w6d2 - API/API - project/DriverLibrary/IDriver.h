#pragma once

#include <string>
#include <memory>



class CReader;

class IDriver
{
public:
	IDriver();
	~IDriver();
	
	virtual std::shared_ptr<CReader> getReader(const std::string& fileName) const = 0;
};

