#include "ProcessUnit.h"
#include "TxtDriver.h"

namespace File
{
	ProcessUnit::ProcessUnit()
	{
		m_drivers.emplace(".txt", new TxtDriver);
	}


	ProcessUnit::~ProcessUnit()
	{
	}

	std::shared_ptr<CReader> ProcessUnit::getReader(const std::string& fileName)
	{
		std::stringstream ss;
		auto it = fileName.begin();
		for(;it != fileName.end(); ++it)
		{
			if(*it == '.')
			{
				while(it != fileName.end())
				{
					ss << *it;
					++it;
				}
				break;
			}
		}
		std::string extension = ss.str();
		return m_drivers[extension]->getReader(fileName);
	}

	void ProcessUnit::addDriver(IDriver* newDriver, const std::string& extension)
	{
		m_drivers.emplace(extension, newDriver);
	}
}

