#pragma once

#ifdef READER_EXPORTS
#define PROCESS_UNIT_API __declspec(dllexport)
#else
#define PROCESS_UNIT_API __declspec(dllimport)
#endif

#include <string>
#include <unordered_map>
#include <sstream>

#include "TxtReaderImpl.h"

class CReader;
class IDriver;

namespace File
{

	class PROCESS_UNIT_API ProcessUnit
	{
	public:
		ProcessUnit();
		~ProcessUnit();

		std::shared_ptr<CReader> getReader(const std::string& fileName);
		void addDriver(IDriver* newDriver, const std::string& extension);

	private:
		std::unordered_map<std::string, IDriver*> m_drivers;
	};	
}
	

