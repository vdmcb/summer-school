#include "TxtDriver.h"
#include "TxtReaderImpl.h"


TxtDriver::TxtDriver()
{
}


TxtDriver::~TxtDriver()
{
}

std::shared_ptr<CReader> TxtDriver::getReader(const std::string& fileName) const
{
	std::cout << "I AM A TXT READER IMPLEMENTATION!" << std::endl;
	return std::make_shared<CReader>(new TxtReaderImpl(fileName));
}
