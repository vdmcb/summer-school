#pragma once

#include <string>

#include "IDriver.h"
#include "CReader.h"

class TxtDriver :
	public IDriver
{
public:
	TxtDriver();
	~TxtDriver();


	std::shared_ptr<CReader> getReader(const std::string& fileName) const override;
};

