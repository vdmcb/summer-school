#pragma once
#include <iostream> 
#include <memory>

#include "CReaderImpl.h"

class TxtReaderImpl :
	public CReaderImpl
{
public:
	TxtReaderImpl(const std::string& fileName);
	~TxtReaderImpl();

	std::string getName() const override;
	std::string getUnit() const override;
	std::string getData() const override;
private:
	std::string m_fileName;
};

