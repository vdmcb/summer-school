#include "yatzy.hpp"
#include <string.h>


std::vector<int> Yatzy::sumOfSameDiceValues(totalNumberOfFaces, 0);

std::vector<int> Yatzy::constructVectorOfSums(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue)
{
	sumOfSameDiceValues = { 0,0,0,0,0,0 };

	sumOfSameDiceValues[diceOneValue - 1] ++;
	sumOfSameDiceValues[diceTwoValue- 1] ++;
	sumOfSameDiceValues[diceThreeValue - 1] ++;
	sumOfSameDiceValues[diceFourValue - 1] ++;
	sumOfSameDiceValues[diceFiveValue - 1] ++;

	return sumOfSameDiceValues;
}

int Yatzy::calculatePairScore(const int valueMultiplier, const std::vector<int>& numberOfEachValue)
{
	for (auto at = 0; at != totalNumberOfFaces; at++)
		if (numberOfEachValue[totalNumberOfFaces - at - 1] == valueMultiplier)
			return (totalNumberOfFaces - at) * valueMultiplier;
	return 0;
}


int Yatzy::Chance(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue)
{

	return diceOneValue + diceTwoValue + diceThreeValue + diceFourValue + diceFiveValue;
}


int Yatzy::yatzy(int dice[])
{
	int counts[totalNumberOfFaces] = { 0,0,0,0,0,0 };
	for (int i = 0; i != 5; i++)
		counts[dice[i] - 1]++;
	for (int i = 0; i != totalNumberOfFaces; i++)
		if (counts[i] == 5)
			return 50;
	return 0;
}

int Yatzy::Ones(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue) {
	sumOfSameDiceValues = { 0,0,0,0,0,0 };

	const int currentFaceValue = 1;
	return constructVectorOfSums(diceOneValue,diceTwoValue,diceThreeValue,diceFourValue,diceFiveValue).at(currentFaceValue - 1) * currentFaceValue;
}

int Yatzy::Twos(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue) {
	sumOfSameDiceValues = { 0,0,0,0,0,0 };

	const int currentFaceValue = 2;
	return constructVectorOfSums(diceOneValue, diceTwoValue, diceThreeValue, diceFourValue, diceFiveValue).at(currentFaceValue - 1) * currentFaceValue;
}


int Yatzy::Threes(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue) {
	sumOfSameDiceValues = { 0,0,0,0,0,0 };

	const int currentFaceValue = 3;
	return constructVectorOfSums(diceOneValue, diceTwoValue, diceThreeValue, diceFourValue, diceFiveValue).at(currentFaceValue - 1) * currentFaceValue;
}

int Yatzy::Fours()
{
	const int currentFaceValue = 4;
	return sumOfValuesFromConstructor.at(currentFaceValue - 1) * currentFaceValue;
}


int Yatzy::Fives()
{
	const int currentFaceValue = 5;
	return sumOfValuesFromConstructor.at(currentFaceValue - 1) * currentFaceValue;
}

int Yatzy::sixes()
{
	const int currentFaceValue = 6;
	return sumOfValuesFromConstructor.at(currentFaceValue - 1) * currentFaceValue;
}
Yatzy::Yatzy()
{
}

Yatzy::Yatzy(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue)
{
	sumOfValuesFromConstructor = constructVectorOfSums(diceOneValue, diceTwoValue, diceThreeValue, diceFourValue, diceFiveValue);
}



int Yatzy::ScorePair(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue)
{
	const int valueMultiplier = 2;
	std::vector<int> currentNumberOfEachValue;
	currentNumberOfEachValue = constructVectorOfSums(diceOneValue, diceTwoValue, diceThreeValue, diceFourValue, diceFiveValue);
	
	return calculatePairScore(valueMultiplier, currentNumberOfEachValue);
}

int Yatzy::TwoPair(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue)
{
	std::vector<int> currentNumberOfEachValue;
	currentNumberOfEachValue = constructVectorOfSums(diceOneValue, diceTwoValue, diceThreeValue, diceFourValue, diceFiveValue);

	int n = 0;
	int score = 0;
	for (int i = 0; i < totalNumberOfFaces; i += 1)
		if (currentNumberOfEachValue[totalNumberOfFaces - i - 1] >= 2) {
			n++;
			score += (totalNumberOfFaces - i);
		}
	if (n == 2)
		return score * 2;
	else
		return 0;
}

int Yatzy::FourOfAKind(int _1, int _2, int diceThreeValue, int diceFourValue, int diceFiveValue)
{
	int * tallies;
	tallies = new int[totalNumberOfFaces];
	tallies[0] = tallies[1] = tallies[2] = 0;
	tallies[3] = tallies[4] = tallies[5] = 0;
	tallies[_1 - 1]++;
	tallies[_2 - 1]++;
	tallies[diceThreeValue - 1]++;
	tallies[diceFourValue - 1]++;
	tallies[diceFiveValue - 1]++;
	for (int i = 0; i < totalNumberOfFaces; i++)
		if (tallies[i] >= 4)
			return (i + 1) * 4;
	return 0;
}

int Yatzy::ThreeOfAKind(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue)
{
	int * t;
	t = new int[totalNumberOfFaces];
	t[0] = t[1] = t[2] = 0;
	t[3] = t[4] = t[5] = 0;
	t[diceOneValue - 1]++;
	t[diceTwoValue - 1]++;
	t[diceThreeValue - 1]++;
	t[diceFourValue - 1]++;
	t[diceFiveValue - 1]++;
	for (int i = 0; i < totalNumberOfFaces; i++)
		if (t[i] >= 3)
			return (i + 1) * 3;
	return 0;
}

int Yatzy::SmallStraight(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue)
{
	int* tallies = new int[totalNumberOfFaces];
	memset(tallies, 0, sizeof(int) * totalNumberOfFaces);
	tallies[diceOneValue - 1] += 1;
	tallies[diceTwoValue - 1] += 1;
	tallies[diceThreeValue - 1] += 1;
	tallies[diceFourValue - 1] += 1;
	tallies[diceFiveValue - 1] += 1;
	if (tallies[0] == 1 &&
		tallies[1] == 1 &&
		tallies[2] == 1 &&
		tallies[3] == 1 &&
		tallies[4] == 1)
		return 15;
	return 0;
}

int Yatzy::LargeStraight(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue)
{
	int* tallies = new int[totalNumberOfFaces];
	memset(tallies, 0, sizeof(*tallies) * totalNumberOfFaces);
	tallies[diceOneValue - 1] += 1;
	tallies[diceTwoValue - 1] += 1;
	tallies[diceThreeValue - 1] += 1;
	tallies[diceFourValue - 1] += 1;
	tallies[diceFiveValue - 1] += 1;
	if (tallies[1] == 1 &&
		tallies[2] == 1 &&
		tallies[3] == 1 &&
		tallies[4] == 1
		&& tallies[5] == 1)
		return 20;
	return 0;
}


int Yatzy::FullHouse(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue)
{
	int* tallies;
	bool _2 = false;
	int i;
	int _2_at = 0;
	bool _3 = false;
	int _3_at = 0;




	tallies = new int[totalNumberOfFaces];
	memset(tallies, 0, sizeof(int) * totalNumberOfFaces);
	tallies[diceOneValue - 1] += 1;
	tallies[diceTwoValue - 1] += 1;
	tallies[diceThreeValue - 1] += 1;
	tallies[diceFourValue - 1] += 1;
	tallies[diceFiveValue - 1] += 1;

	for (i = 0; i != totalNumberOfFaces; i += 1)
		if (tallies[i] == 2) {
			_2 = true;
			_2_at = i + 1;
		}

	for (i = 0; i != totalNumberOfFaces; i += 1)
		if (tallies[i] == 3) {
			_3 = true;
			_3_at = i + 1;
		}

	if (_2 && _3)
		return _2_at * 2 + _3_at * 3;
	else
		return 0;
}
