#ifndef YAHTZEE_INCLUDED
#define YATHZEE_INCLUDED

#include <vector>

class Yatzy
{
public:

	static int Chance(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);
	static int yatzy(int dice[]);
	static int Ones(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);
	static int Twos(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);
	static int Threes(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);

protected:
	int * dice;
public:
	Yatzy();
	Yatzy(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int _5);
	int Fours();
	int Fives();
	int sixes();
	int ScorePair(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);
	static int TwoPair(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);
	static int FourOfAKind(int _1, int _2, int diceThreeValue, int diceFourValue, int diceFiveValue);
	static int ThreeOfAKind(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);

	static std::vector<int> constructVectorOfSums(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);
	static int calculatePairScore(const int valueMultiplier, const std::vector<int>& numberOfEachValue);

	static int SmallStraight(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);
	static int LargeStraight(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);
	static int FullHouse(int diceOneValue, int diceTwoValue, int diceThreeValue, int diceFourValue, int diceFiveValue);

private:
	static std::vector<int> sumOfSameDiceValues;
	std::vector<int> sumOfValuesFromConstructor;

	static const int totalNumberOfFaces = 6;
};

#endif