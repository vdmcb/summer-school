#include "tennis.h"

std::string getDisplayableScoreWhenTie(const int score)
{
	switch (score)
	{
	case 0:
		return "Love-All";
	case 1:
		return "Fifteen-All";
	case 2:
		return "Thirty-All";
	default:
		return "Deuce";
	}
}

std::string getDisplayableScoreWhenAdvantage(const int firstPlayerScore, const int secondPlayerScore)
{
	if (firstPlayerScore > secondPlayerScore)
		return "Advantage player1";
	return "Advantage player2";
}

std::string getDisplayableScoreWhenWinning(const int firstPlayerScore, const int secondPlayerScore)
{
	if (firstPlayerScore > secondPlayerScore)
		return "Win for player1";
	return "Win for player2";
}

bool isTie(int firstPlayerScore, int secondPlayerScore)
{
	return firstPlayerScore == secondPlayerScore;
}

bool isWinningSituation(const int firstPlayerScore, const int secondPlayerScore)
{
	const int minimumWinningPoints = 4;
	bool atLeastOnePLayerHasFourPoints = firstPlayerScore >= minimumWinningPoints || secondPlayerScore >= minimumWinningPoints;
	bool twoOrMorePointsDifference = std::abs(firstPlayerScore - secondPlayerScore) >= 2;

	return atLeastOnePLayerHasFourPoints && twoOrMorePointsDifference;
}

bool isAdvantage(const int firstPlayerScore, const int secondPlayerScore)
{
	bool atLeastOnePlayerHasFourPoints = firstPlayerScore >= 4 || secondPlayerScore >= 4;
	bool onePointDifference = std::abs(firstPlayerScore - secondPlayerScore) == 1;

	return atLeastOnePlayerHasFourPoints && onePointDifference;
}


std::string scoreToString(const int secondPlayerScore)
{
	switch (secondPlayerScore)
	{
	case 0:
		return "Love";
	case 1:
		return "Fifteen";
	case 2:
		return "Thirty";
	case 3:
		return "Forty";
	}
}

std::string getDisplayableScore(const int firstPlayerScore, const int secondPlayerScore)
{
	return scoreToString(firstPlayerScore) + "-" + scoreToString(secondPlayerScore);
}


const std::string tennis_score(int firstPlayerScore, int secondPlayerScore)
{
	if (isTie(firstPlayerScore, secondPlayerScore))
		return getDisplayableScoreWhenTie(firstPlayerScore);
	if (isAdvantage(firstPlayerScore, secondPlayerScore))
	{
		return getDisplayableScoreWhenAdvantage(firstPlayerScore, secondPlayerScore);
	}
	if (isWinningSituation(firstPlayerScore, secondPlayerScore))
	{
		return getDisplayableScoreWhenWinning(firstPlayerScore, secondPlayerScore);
	}

	return getDisplayableScore(firstPlayerScore, secondPlayerScore);
}
